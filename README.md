# Better Privacy Guard

The GUI for macOS, inspired by the [command line tool I previously created](https://gitlab.com/Myl0g/bpg).

## TODO

* Implement key page
* Resolve issues related to pasteboard not working as expected
* Add AppIcon