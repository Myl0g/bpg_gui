import Foundation

func getKeys() -> [String] {
    return runCommand(cmd: "/usr/local/bin/gpg", args: "--list-keys").output
}

func createKeypair(realName: String, email: String) {
    if bpgDebug {
        NSLog("Creating a keypair with name %@ and email %@", realName, email)
    }
    
    makeAndWriteFile(name: "gpg-gen-key-script", content: """
Key-Type: 1
Key-Length: 4096
Subkey-Type: 1
Subkey-Length: 4096
Name-Real: \(realName)
Name-Email: \(email)
Expire-Date: 0
""")

    defer {
        deleteFile(name: "gpg-gen-key-script")
    }
    
    let cmd = runCommand(cmd: "/usr/bin/env", args: "/usr/local/bin/gpg", "--batch", "--gen-key", getAbsoluteFilePath(name: "gpg-gen-key-script")!)
    if bpgDebug {
        NSLog("Output from gpg: %@", cmd.output.joined())
        NSLog("Errors from gpg: %@", cmd.error.joined())
    }
}

func editKeypair() {}

func importKeypair(block: String) {
    if bpgDebug {
        NSLog("Importing keypair %@", block)
    }
    
    makeAndWriteFile(name: "tmp-gpg-keypair", content: block);
    defer {
        deleteFile(name: "tmp-gpg-keypair")
    }
    
    let cmd = runCommand(cmd: "/usr/bin/env", args: "/usr/local/bin/gpg", "--import", getAbsoluteFilePath(name: "tmp-gpg-keypair")!)
    if bpgDebug {
        NSLog("GPG output: %@", cmd.output.joined())
        NSLog("GPG error: %@", cmd.error.joined())
    }
}

func exportKey(keyID: String) -> String {
    if bpgDebug {
        NSLog("Now exporting key with ID %@", keyID)
    }
    
    let cmd = runCommand(cmd: "/usr/bin/env", args: "gpg", "--export", "-a", keyID)
    
    if bpgDebug {
        NSLog("Errors from GPG: %@", cmd.error.joined())
        NSLog("Output: %@", cmd.output.joined())
    }
    
    return cmd.output.joined()
}

func generateKeypairRevocation(keyID _: String) -> String {
    return ""
}

func revokeKeypair(keyID: String) {
    importKeypair(block: generateKeypairRevocation(keyID: keyID))
}
