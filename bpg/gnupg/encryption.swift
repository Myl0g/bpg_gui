import Foundation

func encrypt(message: String, recipients: [String]) -> String {
    if bpgDebug {
        NSLog("Encrypting the following: %@", message)
    }

    var recipientsAsArg: [String] = []
    for recipient in recipients {
        recipientsAsArg.append("-r \(recipient)")
    }

    if bpgDebug {
        NSLog("Recipients for new message: %@", recipientsAsArg.joined())
    }

    makeAndWriteFile(name: "bpg-tmp-encrypt", content: message)
    defer {
        deleteFile(name: "bpg-tmp-encrypt")
        deleteFile(name: "bpg-tmp-encrypt.asc")
    }

    if bpgDebug {
        NSLog("Wrote bpg-tmp-encrypt and deferred deletion of both it and resulting file")
    }

    let args = ["--homedir", "~/.gnupg", "--encrypt", "--sign", "-a"] + recipientsAsArg + ["\(getAbsoluteFilePath(name: "bpg-tmp-encrypt")!)"]
    let err = runCommand(cmd: "/usr/local/bin/gpg", args: args).error

    if bpgDebug {
        NSLog("Got the following errors from gpg: %@", err.joined())
    }

    return readFile(name: "bpg-tmp-encrypt.asc")
}

func decrypt(message: String) -> String {
    makeAndWriteFile(name: "bpg-tmp-decrypt.asc", content: message)
    defer {
        deleteFile(name: "bpg-tmp-decrypt.asc")
        deleteFile(name: "bpg-tmp-decrypt")
    }

    if bpgDebug {
        NSLog("Created bpg-tmp-decrypt.asc and deferred deletion of both it and the decrypted version to be produced")
    }

    let cmd = runCommand(cmd: "/usr/bin/env", args: "/usr/local/bin/gpg", "--homedir", "~/.gnupg", "\(getAbsoluteFilePath(name: "bpg-tmp-decrypt.asc")!)").error

    if bpgDebug {
        NSLog("Errors from gpg: %@", cmd.joined())
    }

    return readFile(name: "bpg-tmp-decrypt")
}
