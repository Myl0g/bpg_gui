import Foundation

func makeAndWriteFile(name: String, content: String) {
    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
        let fileURL = dir.appendingPathComponent(name)
        do {
            try content.write(to: fileURL, atomically: false, encoding: .utf8)
        } catch {}
    }
}

func readFile(name: String) -> String {
    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
        let fileURL = dir.appendingPathComponent(name)
        do {
            return try String(contentsOf: fileURL, encoding: .utf8)
        } catch {
            return ""
        }
    }
    return ""
}

func deleteFile(name: String) {
    let fileManager = FileManager.default

    do {
        try fileManager.removeItem(atPath: getAbsoluteFilePath(name: name)!)
    } catch {}
}

func getAbsoluteFilePath(name: String) -> String? {
    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
        let fileURL = dir.appendingPathComponent(name)
        return fileURL.absoluteString.components(separatedBy: "file://")[1]
    }
    return nil
}
