//
//  ImportKeypairViewController.swift
//  bpg
//
//  Created by Milo Gilad on 2/14/19.
//  Copyright © 2019 Milo Gilad. All rights reserved.
//

import Cocoa

class ImportKeypairViewController: NSViewController {

    @IBOutlet weak var blockField: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    @IBAction func importKeyBlock(_ sender: Any) {
        importKeypair(block: blockField.stringValue)
    }
}
