import Cocoa

class EncryptionViewController: NSViewController {
    @IBOutlet var recipientEmailsField: NSTextField!
    @IBOutlet var messageField: NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        title = "Encrypt"
    }

    @IBAction func encryptMessage(_: NSButton) {
        let msg = encrypt(message: messageField.stringValue, recipients: recipientEmailsField.stringValue.components(separatedBy: " "))
        NSPasteboard.general.clearContents()
        NSPasteboard.general.writeObjects([msg as NSString])
        simpleAlert(message: "The encrypted message has been copied to your clipboard.")
    }
}
