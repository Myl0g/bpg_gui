//
//  GenerateKeypairViewController.swift
//  bpg
//
//  Created by Milo Gilad on 2/14/19.
//  Copyright © 2019 Milo Gilad. All rights reserved.
//

import Cocoa

class GenerateKeypairViewController: NSViewController {

    @IBOutlet weak var fullName: NSTextField!
    @IBOutlet weak var email: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    @IBAction func generateKeypair(_ sender: Any) {
        createKeypair(realName: fullName.stringValue, email: email.stringValue)
    }
}
